#!/bin/sh

tg_msg=$(echo "Pipeline $CI_PIPELINE_ID failed: ❌ Message:$1")
url="https://api.telegram.org/bot${TG_BOT_TOKEN}/sendMessage"
jq -n --arg msg "$tg_msg" --arg chat "$TG_CHAT_ID" '{text: $msg,chat_id: $chat,parse_mode:"Markdown"}' >tg_message.json
curl --url $url --header 'content-type: application/json' --data "@tg_message.json"
rm $(pwd)/tg_message.json
